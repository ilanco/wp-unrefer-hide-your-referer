#!/bin/bash
# A modification of Dean Clatworthy's deploy script as found here: https://github.com/deanc/wordpress-plugin-git-svn
# The difference is that this script lives in the plugin's git repo & doesn't require an existing SVN repo.

# build config
VERSION=$1

# main config
PLUGINSLUG="wp-hide-referer"
CURRENTDIR=`pwd`
MAINFILE="wp-hide-referer.php" # this should be the name of your main php file in the wordpress plugin

# git config
GITPATH="$CURRENTDIR/" # this file should be in the base of your git repository

# svn config
SVNPATH="/tmp/$PLUGINSLUG" # path to a temp SVN repo. No trailing slash required and don't add trunk.
SVNURL="http://plugins.svn.wordpress.org/$PLUGINSLUG/" # Remote SVN repo on wordpress.org, with no trailing slash
SVNUSER="ilanco" # your svn username


if [ -z $VERSION ]; then
    echo "Must enter a version number!"
    echo "Usage: $0 0.1.0"
    exit
fi

echo "Building package for WordPress plugin version: $VERSION"

# Cleanup tmp dir
rm -r $SVNPATH 2>/dev/null
mkdir $SVNPATH

echo "Cleaned up $SVNPATH directory"

# Let's begin...
echo "Preparing to deploy wordpress plugin"

if git show-ref --tags --quiet --verify -- "refs/tags/v$VERSION"
	then 
		echo "Version $VERSION already exists as git tag. Exiting...."; 
		exit 1; 
	else
		echo "Git version does not exist. Let's proceed..."
fi

cd $GITPATH
echo 
echo "Tagging new version in git"
git tag -a "v$VERSION" -m "Tagging version $VERSION"

echo
echo "Pushing latest commit to origin, with tags"
git push all master
git push all master --tags

echo 
echo "Creating local copy of SVN repo ..."
svn co $SVNURL $SVNPATH

echo "Exporting the HEAD of master from git to the trunk of SVN"
git checkout-index -a -f --prefix=$SVNPATH/trunk/

echo "Update version variable"
sed -i "s/%VERSION%/$VERSION/;" $SVNPATH/trunk/readme.txt
sed -i "s/%VERSION%/$VERSION/;" $SVNPATH/trunk/wp-hide-referer.php

echo "Ignoring github specific files and deployment script"
svn propset svn:ignore "deploy.sh
README.md
.git
.gitignore" "$SVNPATH/trunk/"

echo "Changing directory to SVN and committing to trunk"
cd $SVNPATH/trunk/
# Add all new files that are not set to be ignored
svn status | grep -v "^.[ \t]*\..*" | awk '{print $2}' | xargs svn add
svn commit --username=$SVNUSER -m "Version $VERSION"

echo "Creating new SVN tag & committing it"
cd $SVNPATH
svn copy trunk/ tags/$VERSION/
cd $SVNPATH/tags/$VERSION
svn commit --username=$SVNUSER -m "Tagging version $VERSION"

echo "Removing temporary directory $SVNPATH"
rm -fr $SVNPATH/

echo "*** DONE ***"
